<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Application;
use Bitrix\Main\Engine\CurrentUser;
use \Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable;
use \Bitrix\Main\UserTable;

use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\JsonPayload;
use Bitrix\Main\Error;
use Bitrix\Main\Errorable;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Service\GeoIp\Manager;
use Bitrix\Main\Type\DateTime;

class UsersQuestionsAjaxController extends  Bitrix\Main\Engine\Controller
{
	protected $arParams;
	protected $arResult;
	protected $userId;
	protected $errors = array();
	protected $appEntity;
	protected $appEntityId;


	public function __construct($component = null)
	{
		$this->connection = Application::getConnection();
		$this->app = Application::getInstance();
		\Bitrix\Main\Loader::includeModule('highloadblock');

		$this->getEntity();

		parent::__construct($component);
	}

	function getEntity(){
		// найдем для начала хайлоад блок:
		$arHlData = HighloadBlockTable::getList([
			'filter' => [
				'NAME' => [
					'Questions' // Название сущности хайлоад-блока
				]
			],
			'select' => ['*'],
			'cache' => [
				'ttl' => 8640000,
				'cache_joins' => true,
			]
		])->fetch();

		$this->appEntity = HighloadBlockTable::compileEntity($arHlData);

		$this->appEntityId = $arHlData['ID'];

		return $this->appEntity->getDataClass();
	}

	/**
	 * @return array[]
	 */
	public function configureActions():array
	{
		// сбрасываем фильтры по-умолчанию (Bitrix\Main\Engine\ActionFilter\Authentication() и Bitrix\Main\Engine\ActionFilter\HttpMethod() и Bitrix\Main\Engine\ActionFilter\Csrf()), предустановленные фильтры находятся в папке /bitrix/modules/main/lib/engine/actionfilter/

		$defaultPrefilters = [
			'prefilters' => [
				new ActionFilter\Authentication(), // Если нужна авторизация!
				new ActionFilter\HttpMethod([ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST]), // разрешаем и гет и пост
				new ActionFilter\Csrf(), // проверку домена оставляем
			],
			'postfilters' => []
		];

		return [
			'addUserQuestions' => $defaultPrefilters,
		];
	}

	public function addUserQuestionsAction()
	{
		$request = Application::getInstance()->getContext()->getRequest();
		
		$userData = [
			'UF_USER_ID'=>$request->get('user_id'),
			'UF_TEXT'=>$request->get('message')
		];

		$requestHash = md5(serialize($userData)); // получаем хеш-сумму запроса для защиты от спама

		$userData['UF_DATETIME'] = new DateTime();
		$userData['UF_XML_ID'] = $requestHash; // хеш записываем и в базу данных для фильтрации на уровне базы дублей

		$session = \Bitrix\Main\Application::getInstance()->getSession();
		if(!$session->has($requestHash)){
			$entity_data_class = $this->appEntity->getDataClass();
			$res = $entity_data_class::add($userData);
			if(!$res->isSuccess()){
				$this->addErrors($res->getErrors());
				return \Bitrix\Main\Web\Json::encode([
					'status'=>'error',
					'errors'=>$res->getErrors()
				]);
			} else {
				$session->set($requestHash,time());

				$userData['ADMIN_LINK'] = '/admin/highloadblock_rows_list.php='.$this->appEntityId.'&ID='.$res->getId();
				// ссылка на карточку записи в базе данных. Домен сайта уже в шаблоне письма должен быть прописан

				\Bitrix\Main\Mail\Event::send(array(
					'EVENT_NAME' => 'USER_REQUEST', // TODO: создать данное почтовое событие и шаблон письма под него!
					'LID' => 's1',
					'C_FIELDS' => $userData
				));

				return \Bitrix\Main\Web\Json::encode([
					'status'=>'success',
					'resultId'=>$res->getId()
				]);
			}
		} else {
			$this->addError(new Error('Запись уже была добавлена в базу!'));
			return \Bitrix\Main\Web\Json::encode([
				'status'=>'error',
				'errors'=>['Запись уже была добавлена в базу!']
			]);
		}


	}
}