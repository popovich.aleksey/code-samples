var formSubmitting = false;

document.addEventListener('DOMContentLoaded', function () {
    var addQuestionForm = document.querySelector('form[name="addQuestion"]');
    if (addQuestionForm) {
        addQuestionForm.addEventListener('submit', function (event) {
            event.preventDefault();

            if(formSubmitting) {
                return false;
            }

            formSubmitting = true;
            var wait = BX.showWait('requestForm');

            var formData = new FormData(addQuestionForm);
            let config = {mode: 'ajax', data: formData };
            let resultContainer = addQuestionForm.previousElementSibling;

            BX.ajax.runComponentAction('pai:user.questions', 'addUserQuestions', config).then(
                function onOk(event) {
                    addQuestionForm.classList.add('is-hidden'); // скрываем форму
                    addQuestionForm.querySelector('textarea').value = '';
                    resultContainer.classList.remove('is-hidden');
                    resultContainer.children[0].textContent = 'Заявка успешно принята';
                    resultContainer.children[1].textContent = 'Ваша заявка передана на рассмотрение';
                    formSubmitting = false;
                    BX.closeWait('requestForm', wait);
                }, function onFail(event) {
                    let alertContent = ''
                    for (let err of event.errors) {
                        console.log(err)
                        alertContent += `<p>${err.message}</p>`
                    }
                    formSubmitting = false;
                    BX.closeWait('requestForm', wait);
                }
            );

            return false;
        });
    }

});
