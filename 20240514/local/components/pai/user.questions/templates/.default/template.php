<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

use Bitrix\Main\Localization\Loc as Loc;
Loc::loadMessages(__FILE__);

/**
 * @var array $arResult
 * @var array $arParams
 * @global $APPLICATION
 */
?>
<section id="requestForm" class="container">
    <div class='resultContainer is-hidden'>
        <div class="title"></div>
        <div class="message"></div>
    </div>
    <form class="needs-validation" novalidate="" name="addQuestion">
        <input type="hidden" name="user_id" value="<?=$arParams['CUR_USER_ID'];?>">
        <div class="row g-3">
            <div class="col-12">
                <label for="message" class="form-label"><?php echo Loc::getMessage('LABEL_MESSAGE')?>:</label>
                <textarea name="message" id="message" class="form-control"></textarea>
                <div class="invalid-feedback">
                <?php echo Loc::getMessage('LABEL_MESSAGE_INVALID')?>
                </div>
            </div>
        </div>
        <button class="w-100 btn btn-primary btn-lg" type="submit"><?php echo Loc::getMessage('SEND_BUTTON')?></button>
    </form>
</section>