<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

// класс для работы с языковыми файлами
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Engine\CurrentUser;
use \Bitrix\Main\Type\DateTime;


use Bitrix\Main\Application;
use \Bitrix\Main\Data\Cache;


Loc::loadMessages(__FILE__);

class UserQuestions extends CBitrixComponent
{

	var $questEntity;

	public function executeComponent()
	{
		try {
			$this->checkModules();
			$this->IncludeComponentTemplate();
		}
		catch (SystemException $e) {
			ShowError($e->getMessage());
		}
	}

	public function onIncludeComponentLang() : void
	{
		Loc::loadMessages(__FILE__);
	}

	/**
	 * проверяем установку модуля «Информационные блоки» (метод подключается внутри класса try...catch)
	 */
	protected function checkModules() : void
	{
		// если модуль не подключен
		if (!Loader::includeModule('highloadblock'))
		{
			// выводим сообщение в catch
			throw new SystemException(Loc::getMessage('HIGHLOAD_BLOCK_MODULE_NOT_INSTALLED'));
		}
	}

	/**
	 * обработка массива $arParams (метод подключается автоматически)
	 *
	 * @param $arParams
	 * @return array
	 */
	public function onPrepareComponentParams($arParams)
	{
		// время кеширования
		if (!isset($arParams['CACHE_TIME']))
		{
			$arParams['CACHE_TIME'] = 3600;
		} else
		{
			$arParams['CACHE_TIME'] = intval($arParams['CACHE_TIME']);
		}

		$arParams['CUR_USER_ID'] = (int)CurrentUser::get()->getId();

		// возвращаем в метод новый массив $arParams
		return $arParams;
	}
}