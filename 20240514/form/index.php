<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $USER;
if(!$USER->IsAuthorized())
{
    LocalRedirect('/auth/?backurl=/form/');
}
/** @var object $APPLICATION */
$APPLICATION->SetTitle("Форма запроса данных от пользователя");
?>
<?php
$APPLICATION->IncludeComponent(
	"pai:user.questions", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);
?>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>