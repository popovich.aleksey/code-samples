<?php
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
?>
<script>
	<?php if (isset($_REQUEST["backurl"]) && strlen($_REQUEST["backurl"])>0 && preg_match('#^/\w#', $_REQUEST["backurl"])):?>
		document.location.href = "<?=CUtil::JSEscape($_REQUEST["backurl"])?>";
	<?php else:?>
		document.location.href = "/form/";
  <?php endif?>
</script>

<?php $APPLICATION->SetTitle("Вы зарегистрированы и успешно авторизовались.");?>
<h1>Авторизация</h1>
<p>Вы зарегистрированы и успешно авторизовались.</p>
<p><a href="<?=SITE_DIR?>" class="backToIndexPage">Вернуться на главную страницу</a></p>
<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>